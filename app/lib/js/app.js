var dataURL = './colors.json';
var xhr = new XMLHttpRequest();
var self = this;

var demo = new Vue({
    el: '#app',
    data: {
        appName: "Color Set",
        colors: null
    },
    created() {
        this.fetchData();
    },
    methods: {
        fetchData() {
            xhr.open("GET", dataURL, true);
            xhr.setRequestHeader("content-type", "application/json; charset=utf-8");
            xhr.onload = function() {
                self.colors = JSON.parse(xhr.responseText);
                console.log(self.colors[0]);
            }
            xhr.send(null);
        }
    }
})
