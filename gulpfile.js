var   gulp          = require('gulp');
var   plumber       = require('gulp-plumber');
var   autoprefixer  = require('gulp-autoprefixer');
var   less          = require('gulp-less');

// LESS Compile Task
gulp.task('less', function() {
    var prefixer = autoprefixer({ browsers: ['last 2 versions'] });

    return gulp.src(['app/lib/less/style.less'])
        .pipe(plumber({
            errorHandler: function(error) {
                console.log(error.message);
                this.emit('end');
            }
        }))
        .pipe(less())
        .pipe(prefixer)
        .pipe(gulp.dest('app/lib/css'));
});


// Default Gulp Task
gulp.task('default', function() {
    gulp.watch("app/lib/less/**/*.less", ['less']); // watch .LESS directory for changes.
});
